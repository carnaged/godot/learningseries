extends KinematicBody2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
const SPEED = 80
var player
var drag = 0.4
const GRAVITY = 20
const JUMP_FORCE = -450
var dir = 0
var new_dir = 0
var velocity = Vector2()
const OFFSET_x = 30
const OFFSET_y = 40
const WAIT_TIME = 300
var tick_timeout = 0
var distance = 0
var height_diff = 0

var jump_timeout = -1
var jump_queued = true

func jump():
	if self.is_on_floor():
		velocity.y = JUMP_FORCE
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	player = get_parent().get_node("Player")
	pass # Replace with function body.
	
func queuedir(dir):
	if new_dir != dir:
		new_dir = dir
		tick_timeout = OS.get_ticks_msec() + WAIT_TIME
func _physics_process(delta: float) -> void:
	distance = player.position.x - position.x
	
	# Height diff calculated as positive for sanity :)
	height_diff = position.y - player.position.y
	
	if distance > OFFSET_x:
		queuedir(1)
	elif -1 * distance > OFFSET_x:
		queuedir(-1)
	else:
		dir = 0
	
		
	if  height_diff > OFFSET_y and !jump_queued and !$RayCast2D.is_colliding():
		jump_timeout = OS.get_ticks_msec() + WAIT_TIME
		jump_queued = true
	if is_on_wall() and !jump_queued and !$RayCast2D.is_colliding():
		jump_timeout = OS.get_ticks_msec() + WAIT_TIME
		jump_queued = true
		
	if OS.get_ticks_msec() > tick_timeout:
		dir = new_dir
	if OS.get_ticks_msec() > jump_timeout and jump_queued:
		# abs distance added to stop jumping when near but try if far
		# without it, plain wall scenario would fail because no height difference
		if is_on_floor() and (abs(height_diff) > OFFSET_y or abs(distance) > OFFSET_x):
			jump()
		jump_queued = false
	
	
	$Sprite.flip_h = !bool(dir+1)
	velocity.y += GRAVITY
	
	velocity.x = dir * SPEED
	velocity = move_and_slide(velocity, Vector2.UP)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
